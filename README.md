﻿### Installation
```
git clone <this.repo>
composer install
```

- Setup your pgsql database in .env file
- Run your migrations
- Run the app on your server or docker and visit *your.url/register* to create a few users
- Token grants will be vailable on *your.url/api/accessToken*

