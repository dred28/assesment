<?php
namespace App\Infrastructure\oAuth2Server\Bridge;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

final class ClientRepository implements ClientRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getClientEntity(
        $clientIdentifier,
        $grantType = null,
        $clientSecret = null,
        $mustValidateSecret = true
    ): ?ClientEntityInterface {
        $appClient = $this->appClientRepository->findActive($clientIdentifier);
    	if ($appClient === null) {
        	return null;
    	}
    	if ($mustValidateSecret && !hash_equals($appClient->getSecret(), (string)$clientSecret)) {
        	return null;
    	}
    	$oauthClient = new Client($clientIdentifier, $appClient->getName(), $appClient->getRedirect());
    	return $oauthClient; 
    }

    public function validateClient($clientIdentifier, $clientSecret, $grantType){
    	return true;
    }
}