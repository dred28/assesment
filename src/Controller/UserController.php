<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();

        return $this->render('user/index.html.twig', ['users' => $users]);
    }

    public function showRegister(){
    	return $this->render('user/register.html.twig');	
    }


    public function saveUser(Request $request, UserPasswordEncoderInterface $encoder){
    	$entityManager = $this->getDoctrine()->getManager();

    	$user = new User();
    	$user->setName($request->request->get('name'));
    	$user->setEmail($request->request->get('email'));
    	//$pass = $encoder->encodePassword($user, $request->request->get('password'));
    	$user->setPassword($request->request->get('password'));

    	$entityManager->persist($user);
    	$entityManager->flush();

    	return redirect('/');
    	//return new Response('Saved new product with id '.$user->getId());
    }
}
