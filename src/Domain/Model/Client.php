<?php
namespace App\Domain\Model;

use Ramsey\Uuid\Uuid;

class Client
{
    /**
     * @var string
     */
    private $id;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $secret;
    
    /**
     * @var string
     */
    
    private $redirect;
    /**
     * @var bool
     */
    
    private $active;
    /**
     * Client constructor.
     * @param ClientId $clientId
     * @param string $name
     */
    
    private function __construct(ClientId $clientId, string $name)
    {
        $this->id = $clientId->toString();
        $this->name = $name;
    }
    
    public static function create(string $name): Client
    {
        $clientId = ClientId::fromString(Uuid::uuid4()->toString());
        return new self($clientId, $name);
    }
    
    public function getId(): UserId
    {
        return UserId::fromString($this->id);
    }
   
   public function setName($name): self
   {
   		$this->name = $name;
   		return $this;
   }

   public function getName(){
   		return $this->name;
   }

   public function setSecret($secret): self
   {
   		$this->secret = $secret;
   		return $this;
   }

   public function getSecret(){
   		return $this->secret;
   }

   public function setRedirec($redirect): self
   {
   		$this->redirect = $redirect;
   		return $this;
   }

   public function getRedirect(){
   		return $this->redirect;
   }

   public function setActive($active): self
   {
   		$this->active = $active;
   		return $this;
   }

   public function getActivie(){
   		return $this->active;
   }
}